package by.seleznev.airlines.logic;

import java.io.File;
import org.apache.log4j.Logger;
import by.seleznev.airlines.entity.Airlines;
import by.seleznev.airlines.parser.ParserAirlines;

public class AirlineCreator {

	static Logger logger = Logger.getLogger(AirlineCreator.class);

	public Airlines createAirlines(String name) {
		logger.info("New airlines has been created with name: " + name);
		return new Airlines(name);
	}

	public void completeAirline(Airlines airlines) {
		File jsonFile = new File("resourses/airplanes.json");
		if (jsonFile.exists() && !jsonFile.isDirectory()) {
			ParserAirlines parserAirlines = new ParserAirlines();
			parserAirlines.parseJson(airlines, jsonFile);
		} else {
			throw new RuntimeException();
		}
	}
}
