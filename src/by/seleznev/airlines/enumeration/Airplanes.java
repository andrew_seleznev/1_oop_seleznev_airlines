package by.seleznev.airlines.enumeration;

public enum Airplanes {
	
	PASSENGER("passenger"), CARGO("cargo");
	
	private String name;

	private Airplanes(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return "name: " + getName();
	}
}
