package by.seleznev.airlines.report;

import java.util.ArrayList;
import java.util.List;
import by.seleznev.airlines.entity.Airlines;
import by.seleznev.airlines.entity.Airplane;
import by.seleznev.airlines.service.CountAirlines;
import by.seleznev.airlines.service.SearchAirlines;
import by.seleznev.airlines.service.SortAirlines;
import by.seleznev.airlines.service.comparator.AirlinesRangeComparator;

public class ReportAirlines {

	public static void printAirlines(Airlines airlines) {
		System.out.println("===== " + airlines.getName() + " all airplanes =====");
		for (Airplane airplane : airlines.getListAirplanes()) {
			System.out.println("Airplane: " + airplane.toString());
		}
	}

	public static void printTotalCapacity(Airlines airlines) {
		System.out.println("Total capacity: " + CountAirlines.countTotalCapacity(airlines));
	}

	public static void printTotalPayload(Airlines airlines) {
		System.out.println("Total payload: " + CountAirlines.countTotalPayload(airlines));
	}

	public static void printSortRangeAirlines(Airlines airlines) {
		List<Airplane> sortedAirplanes = SortAirlines.sortRangeAirlines(airlines, new AirlinesRangeComparator());
		System.out.println("===== sorted by range airplanes =====");
		for (Airplane airplane : sortedAirplanes) {
			System.out.println("Airplane: " + airplane.toString());
		}
	}

	public static void printSearchFuelEconomyAirlines(Airlines airlines, double fuelEconomyFrom, double fuelEconomyTo) {
		ArrayList<Airplane> searchAirplanes = SearchAirlines.searchFuelEconomyAirlines(airlines, fuelEconomyFrom,
				fuelEconomyTo);
		System.out.println("===== found airplanes with fuel economy in range from " + fuelEconomyFrom + " to "
				+ fuelEconomyTo + " =====");
		for (Airplane airplane : searchAirplanes) {
			System.out.println("Airplane: " + airplane.toString());
		}
	}
}
