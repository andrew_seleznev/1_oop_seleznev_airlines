package by.seleznev.airlines.entity;

public class PassengerAirplane extends Airplane {

	private int altitude;

	public PassengerAirplane() {

	}

	public PassengerAirplane(String idAirplane, String name, int capacity, int payload, int range, double fuelEconomy,
			int altitude) {
		super(idAirplane, name, capacity, payload, range, fuelEconomy);
		this.altitude = altitude;
	}

	public int getAltitude() {
		return altitude;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + altitude;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PassengerAirplane other = (PassengerAirplane) obj;
		if (altitude != other.altitude)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getSimpleName()).append(super.toString()).append(", altitude: ")
				.append(getAltitude());
		return sb.toString();
	}

}
