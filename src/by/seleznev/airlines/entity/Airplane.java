package by.seleznev.airlines.entity;

public abstract class Airplane {

	private String idAirplane;
	private String name;
	private int capacity;
	private int payload;
	private int range;
	private double fuelEconomy;

	public Airplane(String idAirplane, String name, int capacity, int payload, int range, double fuelEconomy) {
		this.idAirplane = idAirplane;
		this.name = name;
		this.capacity = capacity;
		this.payload = payload;
		this.range = range;
		this.fuelEconomy = fuelEconomy;
	}

	public Airplane() {

	}

	public String getIdAirplane() {
		return idAirplane;
	}

	public String getName() {
		return name;
	}

	public int getCapacity() {
		return capacity;
	}

	public int getPayload() {
		return payload;
	}

	public int getRange() {
		return range;
	}

	public double getFuelEconomy() {
		return fuelEconomy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + capacity;
		long temp;
		temp = Double.doubleToLongBits(fuelEconomy);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((idAirplane == null) ? 0 : idAirplane.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + payload;
		result = prime * result + range;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Airplane other = (Airplane) obj;
		if (capacity != other.capacity)
			return false;
		if (Double.doubleToLongBits(fuelEconomy) != Double.doubleToLongBits(other.fuelEconomy))
			return false;
		if (idAirplane == null) {
			if (other.idAirplane != null)
				return false;
		} else if (!idAirplane.equals(other.idAirplane))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (payload != other.payload)
			return false;
		if (range != other.range)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(" registration : ").append(getIdAirplane()).append(", name: ").append(getName())
				.append(", capacity: ").append(getCapacity());
		sb.append(", payload: ").append(getPayload()).append(", range: ").append(getRange()).append(", fuelEconomy: ")
				.append(getFuelEconomy());
		return sb.toString();
	}

}
