package by.seleznev.airlines.entity.builder;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import by.seleznev.airlines.entity.PassengerAirplane;
import by.seleznev.airlines.exception.IllegalAirplaneArgumentException;

public class PassengerAirplaneBuilder {
	
	private String idAirplane;
	private String name;
	private int capacity;
	private int payload;
	private int range;
	private int altitude;
	private double fuelEconomy;
	
	static Logger logger = Logger.getLogger(PassengerAirplaneBuilder.class);
	
	public boolean isValid() {
		try {
			ResourceBundle bundle = ResourceBundle.getBundle("property/text");
			int minCapacity = Integer.parseInt(bundle.getString("MIN_CAPACITY"));
			int maxCapacity = Integer.parseInt(bundle.getString("MAX_CAPACITY"));
			int minPayload = Integer.parseInt(bundle.getString("MIN_PAYLOAD"));
			int maxPayload = Integer.parseInt(bundle.getString("MAX_PAYLOAD"));
			int minRange = Integer.parseInt(bundle.getString("MIN_RANGE"));
			int maxRange = Integer.parseInt(bundle.getString("MAX_RANGE"));
			
			int minAltitude = Integer.parseInt(bundle.getString("MIN_ALTITUDE"));
			int maxAltitude = Integer.parseInt(bundle.getString("MAX_ALTITUDE"));

			double minFuelEconomy = Double.parseDouble(bundle.getString("MIN_FUEL_ECONOMY"));
			double maxFuelEconomy = Double.parseDouble(bundle.getString("MAX_FUEL_ECONOMY"));
			
			Pattern p = Pattern.compile("[a-zA-Z]{2,5}[-][0-9]{2,5}");
			Matcher m = p.matcher(idAirplane);

			if (idAirplane == null || name == null || capacity == 0 || payload == 0 || range == 0 || fuelEconomy == 0
					|| altitude == 0) {
				return false;
			}

			if (capacity < minCapacity || capacity > maxCapacity) {
				return false;
			}

			if (payload < minPayload || payload > maxPayload) {
				return false;
			}

			if (range < minRange || range > maxRange) {
				return false;
			}

			if (fuelEconomy < minFuelEconomy || fuelEconomy > maxFuelEconomy) {
				return false;
			}

			if (altitude < minAltitude || altitude > maxAltitude) {
				return false;
			}
			if (!m.matches()) {
				return false;
			}

		} catch (MissingResourceException e) {
			throw new RuntimeException();
		} catch (NumberFormatException e) {
			logger.error(e.getMessage());
			return false;
		}

		return true;
	}
	
	public PassengerAirplaneBuilder idAirplane(String idAirplane) {
		this.idAirplane = idAirplane;
		return this;
	}

	public PassengerAirplaneBuilder name(String name) {
		this.name = name;
		return this;
	}

	public PassengerAirplaneBuilder capacity(int capacity) {
		this.capacity = capacity;
		return this;
	}

	public PassengerAirplaneBuilder payload(int payload) {
		this.payload = payload;
		return this;
	}

	public PassengerAirplaneBuilder range(int range) {
		this.range = range;
		return this;
	}
	
	public PassengerAirplaneBuilder altitude(int altitude) {
		this.altitude = altitude;
		return this;
	}

	public PassengerAirplaneBuilder fuelEconomy(double fuelEconomy) {
		this.fuelEconomy = fuelEconomy;
		return this;
	}
	
	public PassengerAirplane build() throws IllegalAirplaneArgumentException {
		if (isValid()) {
			return new PassengerAirplane(idAirplane, name, capacity, payload, range, fuelEconomy, altitude);
		} else {
			throw new IllegalAirplaneArgumentException("Illegal arguments for airplane creation");
		}
	}

	public String getIdAirplane() {
		return idAirplane;
	}

	public String getName() {
		return name;
	}

	public int getCapacity() {
		return capacity;
	}

	public int getPayload() {
		return payload;
	}

	public int getRange() {
		return range;
	}

	public int getAltitude() {
		return altitude;
	}

	public double getFuelEconomy() {
		return fuelEconomy;
	}

	public static Logger getLogger() {
		return logger;
	}

}
