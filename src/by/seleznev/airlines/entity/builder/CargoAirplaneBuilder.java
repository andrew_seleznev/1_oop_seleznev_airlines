package by.seleznev.airlines.entity.builder;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import by.seleznev.airlines.entity.CargoAirplane;
import by.seleznev.airlines.exception.IllegalAirplaneArgumentException;

public class CargoAirplaneBuilder {

	private String idAirplane;
	private String name;
	private int capacity;
	private int payload;
	private int range;
	private double fuelEconomy;
	private double cargoVolume;

	static Logger logger = Logger.getLogger(CargoAirplaneBuilder.class);

	public boolean isValid() {
		try {
			ResourceBundle bundle = ResourceBundle.getBundle("property/text");
			int minCapacity = Integer.parseInt(bundle.getString("MIN_CAPACITY"));
			int maxCapacity = Integer.parseInt(bundle.getString("MAX_CAPACITY"));
			int minPayload = Integer.parseInt(bundle.getString("MIN_PAYLOAD"));
			int maxPayload = Integer.parseInt(bundle.getString("MAX_PAYLOAD"));
			int minRange = Integer.parseInt(bundle.getString("MIN_RANGE"));
			int maxRange = Integer.parseInt(bundle.getString("MAX_RANGE"));

			double minFuelEconomy = Double.parseDouble(bundle.getString("MIN_FUEL_ECONOMY"));
			double maxFuelEconomy = Double.parseDouble(bundle.getString("MAX_FUEL_ECONOMY"));

			double minCargoVolume = Double.parseDouble(bundle.getString("MIN_CARGO_VOLUME"));
			double maxCargoVolume = Double.parseDouble(bundle.getString("MAX_CARGO_VOLUME"));

			Pattern p = Pattern.compile("[a-zA-Z]{2,5}[-][0-9]{2,5}");
			Matcher m = p.matcher(idAirplane);

			if (idAirplane == null || name == null || capacity == 0 || payload == 0 || range == 0 || fuelEconomy == 0
					|| cargoVolume == 0) {
				return false;
			}

			if (capacity < minCapacity || capacity > maxCapacity) {
				return false;
			}

			if (payload < minPayload || payload > maxPayload) {
				return false;
			}

			if (range < minRange || range > maxRange) {
				return false;
			}

			if (fuelEconomy < minFuelEconomy || fuelEconomy > maxFuelEconomy) {
				return false;
			}

			if (cargoVolume < minCargoVolume || cargoVolume > maxCargoVolume) {
				return false;
			}
			if (!m.matches()) {
				return false;
			}

		} catch (MissingResourceException e) {
			throw new RuntimeException();
		} catch (NumberFormatException e) {
			logger.error(e.getMessage());
			return false;
		}

		return true;
	}

	public CargoAirplaneBuilder idAirplane(String idAirplane) {
		this.idAirplane = idAirplane;
		return this;
	}

	public CargoAirplaneBuilder name(String name) {
		this.name = name;
		return this;
	}

	public CargoAirplaneBuilder capacity(int capacity) {
		this.capacity = capacity;
		return this;
	}

	public CargoAirplaneBuilder payload(int payload) {
		this.payload = payload;
		return this;
	}

	public CargoAirplaneBuilder range(int range) {
		this.range = range;
		return this;
	}

	public CargoAirplaneBuilder fuelEconomy(double fuelEconomy) {
		this.fuelEconomy = fuelEconomy;
		return this;
	}

	public CargoAirplaneBuilder cargoVolume(double cargoVolume) {
		this.cargoVolume = cargoVolume;
		return this;
	}

	public CargoAirplane build() throws IllegalAirplaneArgumentException {
		if (isValid()) {
			return new CargoAirplane(idAirplane, name, capacity, payload, range, fuelEconomy, cargoVolume);
		} else {
			throw new IllegalAirplaneArgumentException("Illegal arguments for airplane creation");
		}
	}

	public String getIdAirplane() {
		return idAirplane;
	}

	public String getName() {
		return name;
	}

	public int getCapacity() {
		return capacity;
	}

	public int getPayload() {
		return payload;
	}

	public int getRange() {
		return range;
	}

	public double getFuelEconomy() {
		return fuelEconomy;
	}

	public double getCargoVolume() {
		return cargoVolume;
	}
}
