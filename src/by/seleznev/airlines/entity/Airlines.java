package by.seleznev.airlines.entity;

import java.util.HashSet;

public class Airlines {

	private String name;
	private HashSet<Airplane> listAirplanes;

	public Airlines(String name) {
		listAirplanes = new HashSet<>();
		this.name = name;
	}

	public void addAirplane(Airplane airplane) {
		listAirplanes.add(airplane);
	}

	public HashSet<Airplane> getListAirplanes() {
		return listAirplanes;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "Airlines [name=" + name + "]";
	}
}
