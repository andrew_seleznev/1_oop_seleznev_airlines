package by.seleznev.airlines.entity;

public class CargoAirplane extends Airplane {

	private double cargoVolume;

	public CargoAirplane() {

	}

	public CargoAirplane(String idAirplane, String name, int capacity, int payload, int range, double fuelEconomy,
			double cargoVolume) {
		super(idAirplane, name, capacity, payload, range, fuelEconomy);
		this.cargoVolume = cargoVolume;
	}

	public double getCargoVolume() {
		return cargoVolume;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(cargoVolume);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CargoAirplane other = (CargoAirplane) obj;
		if (Double.doubleToLongBits(cargoVolume) != Double.doubleToLongBits(other.cargoVolume))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getSimpleName()).append(super.toString()).append(", cargoVolume: ")
				.append(getCargoVolume());
		return sb.toString();
	}
}
