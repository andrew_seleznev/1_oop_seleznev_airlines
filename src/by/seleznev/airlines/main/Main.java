package by.seleznev.airlines.main;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import by.seleznev.airlines.entity.Airlines;
import by.seleznev.airlines.logic.AirlineCreator;
import by.seleznev.airlines.report.ReportAirlines;

public class Main {
	
	static {
		new DOMConfigurator().doConfigure("conf/log4j.xml", LogManager.getLoggerRepository());
	}
	
	static Logger logger = Logger.getLogger(Main.class);
	
	public static void main(String[] args) {
		AirlineCreator airlineCreator = new AirlineCreator();
		Airlines airlines = airlineCreator.createAirlines("American Airlines");
		airlineCreator.completeAirline(airlines);
		
		ReportAirlines.printAirlines(airlines);
		ReportAirlines.printTotalCapacity(airlines);
		ReportAirlines.printTotalPayload(airlines);
		ReportAirlines.printSortRangeAirlines(airlines);
		ReportAirlines.printSearchFuelEconomyAirlines(airlines, 10, 30);
		
	}

}
