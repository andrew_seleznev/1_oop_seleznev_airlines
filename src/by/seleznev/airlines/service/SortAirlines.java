package by.seleznev.airlines.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import by.seleznev.airlines.entity.Airlines;
import by.seleznev.airlines.entity.Airplane;
import by.seleznev.airlines.service.comparator.AirlinesRangeComparator;

public class SortAirlines {

	public static List<Airplane> sortRangeAirlines(Airlines airlines, AirlinesRangeComparator comparator) {
		HashSet<Airplane> airplanes = airlines.getListAirplanes();
		ArrayList<Airplane> sortList = new ArrayList<>(airplanes);
		Collections.sort(sortList, comparator);
		return sortList;
	}

}
