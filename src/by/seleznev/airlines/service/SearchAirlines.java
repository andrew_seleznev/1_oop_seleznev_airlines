package by.seleznev.airlines.service;

import java.util.ArrayList;
import by.seleznev.airlines.entity.Airlines;
import by.seleznev.airlines.entity.Airplane;

public class SearchAirlines {

	public static ArrayList<Airplane> searchFuelEconomyAirlines(Airlines airlines, double fuelEconomyFrom,
			double fuelEconomyTo) {
		ArrayList<Airplane> result = new ArrayList<>();
		for (Airplane airplane : airlines.getListAirplanes()) {
			if (airplane.getFuelEconomy() >= fuelEconomyFrom && airplane.getFuelEconomy() <= fuelEconomyTo) {
				result.add(airplane);
			}
		}
		return result;
	}

}
