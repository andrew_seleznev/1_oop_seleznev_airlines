package by.seleznev.airlines.service;

import by.seleznev.airlines.entity.Airlines;
import by.seleznev.airlines.entity.Airplane;

public class CountAirlines {

	public static int countTotalCapacity(Airlines airlines) {
		int totalCapacity = 0;
		for (Airplane airplane : airlines.getListAirplanes()) {
			totalCapacity += airplane.getCapacity();
		}
		return totalCapacity;
	}

	public static int countTotalPayload(Airlines airlines) {
		int totalPayload = 0;
		for (Airplane airplane : airlines.getListAirplanes()) {
			totalPayload += airplane.getPayload();
		}
		return totalPayload;
	}
}
