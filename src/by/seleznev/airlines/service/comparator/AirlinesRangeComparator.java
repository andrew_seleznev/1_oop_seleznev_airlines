package by.seleznev.airlines.service.comparator;

import java.util.Comparator;

import by.seleznev.airlines.entity.Airplane;

public class AirlinesRangeComparator implements Comparator<Airplane> {

	@Override
	public int compare(Airplane o1, Airplane o2) {
		if (o1.getRange() > o2.getRange()) {
			return 1;
		} else if (o1.getRange() < o2.getRange()) {
			return -1;
		} else {
			return 0;
		}
	}
}
