package by.seleznev.airlines.parser;

import java.io.File;
import java.io.IOException;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;

import by.seleznev.airlines.entity.Airlines;
import by.seleznev.airlines.entity.CargoAirplane;
import by.seleznev.airlines.entity.PassengerAirplane;
import by.seleznev.airlines.entity.builder.CargoAirplaneBuilder;
import by.seleznev.airlines.entity.builder.PassengerAirplaneBuilder;
import by.seleznev.airlines.enumeration.Airplanes;
import by.seleznev.airlines.exception.IllegalAirplaneArgumentException;

public class ParserAirlines {

	static Logger logger = Logger.getLogger(ParserAirlines.class);

	public void parseJson(Airlines airlines, File jsonFile) {
		ObjectMapper mapper = new ObjectMapper();

		assert(jsonFile == null);

		JsonFactory factory = new JsonFactory();
		try {
			JsonParser parser = factory.createParser(jsonFile);
			while (!parser.isClosed()) {
				JsonToken token = parser.nextToken();
				if (token == null) {
					break;
				}
				if (JsonToken.FIELD_NAME.equals(token)
						&& Airplanes.PASSENGER.getName().equals(parser.getCurrentName())) {
					token = parser.nextToken();
					if (JsonToken.START_ARRAY.equals(token)) {
						while (parser.nextToken() != JsonToken.END_ARRAY) {
							try {
								PassengerAirplane pAirplane = mapper.readValue(parser, PassengerAirplane.class);
								PassengerAirplane airplane = new PassengerAirplaneBuilder()
										.idAirplane(pAirplane.getIdAirplane()).name(pAirplane.getName())
										.capacity(pAirplane.getCapacity()).payload(pAirplane.getPayload())
										.range(pAirplane.getRange()).altitude(pAirplane.getAltitude())
										.fuelEconomy(pAirplane.getFuelEconomy()).build();
								airlines.addAirplane(airplane);
								logger.info("Passenger airplane has been sucsesfully created and added into the "
										+ airlines.getName());
							} catch (InvalidFormatException e) {
								logger.error("Not a valid values in .json file. " + e.getMessage());
								parser.nextToken();
							} catch (UnrecognizedPropertyException e) {
								logger.error("Unrecognized field has been found" + e.getMessage());
								parser.nextToken();
							} catch (IllegalAirplaneArgumentException e) {
								logger.error("Airplane hasn't been created", e);
							}
						}
					}
				} else if (JsonToken.FIELD_NAME.equals(token)
						&& Airplanes.CARGO.getName().equals(parser.getCurrentName())) {
					token = parser.nextToken();
					if (JsonToken.START_ARRAY.equals(token)) {
						while (parser.nextToken() != JsonToken.END_ARRAY) {
							try {
								CargoAirplane pAirplane = mapper.readValue(parser, CargoAirplane.class);
								CargoAirplane airplane = new CargoAirplaneBuilder()
										.idAirplane(pAirplane.getIdAirplane()).name(pAirplane.getName())
										.capacity(pAirplane.getCapacity()).payload(pAirplane.getPayload())
										.range(pAirplane.getRange()).cargoVolume(pAirplane.getCargoVolume())
										.fuelEconomy(pAirplane.getFuelEconomy()).build();
								airlines.addAirplane(airplane);
								logger.info("Cargo airplane has been sucsesfully created and added into the "
										+ airlines.getName());
							} catch (InvalidFormatException e) {
								logger.error("not a valid values in .json file. " + e.getMessage());
								parser.nextToken();
							} catch (UnrecognizedPropertyException e) {
								logger.error("Unrecognized field has been found" + e.getMessage());
								parser.nextToken();
							} catch (IllegalAirplaneArgumentException e) {
								logger.error("Airplane hasn't been created", e);
							}
						}
					}
				}
			}
		} catch (JsonParseException e) {
			logger.error(e.getMessage());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}
}
