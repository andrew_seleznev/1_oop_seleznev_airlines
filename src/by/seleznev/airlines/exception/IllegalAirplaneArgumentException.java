package by.seleznev.airlines.exception;

public class IllegalAirplaneArgumentException extends Exception {

	private static final long serialVersionUID = 1L;

	public IllegalAirplaneArgumentException() {
		super();
	}
	
	public IllegalAirplaneArgumentException(String message) {
		super(message);
	}
	
	public IllegalAirplaneArgumentException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public IllegalAirplaneArgumentException(Throwable cause) {
		super(cause);
	}
}
